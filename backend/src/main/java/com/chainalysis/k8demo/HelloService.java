package com.chainalysis.k8demo;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller("/")
public class HelloService {
    @Get ("/api")
    @Produces(MediaType.TEXT_HTML)
    public String index() {
        return "<html><body>Hello martino 7";
    }

    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public String getRoot() {
        return "[]";
    }
}
