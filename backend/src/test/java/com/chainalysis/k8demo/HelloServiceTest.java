package com.chainalysis.k8demo;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import javax.inject.Inject;

@MicronautTest
public class HelloServiceTest {

    @Inject
    HelloService helloService;

    @Test
    void testItWorks() {
        Assertions.assertTrue(helloService.index().contains("Hello"));
    }

}
